<?php

use Illuminate\Http\Request;
use App\Message;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::post('user/register', 'APIRegisterController@register');
//Route::post('user/login', 'APILoginController@login');

Route::post('robot/register', 'APIRobotController@register');
Route::post('robot/token', 'APIRobotController@generateToken');

Route::middleware('jwt.auth')->get('users', function(Request $request) {
   return auth()->user();
});

Route::post('messages', 'MessageController@list');
Route::post('messages/create', 'MessageController@create');

Route::post('logs', 'LogMessageController@list');
Route::post('logs/create', 'LogMessageController@create');



