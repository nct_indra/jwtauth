<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LogMessage;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;

class LogMessageController extends Controller
{
    public function __construct(){
        //$this->middleware('jwt.auth');
    }
    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'message_type' => 'required',
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $obj = [
            'message_type' => $request->get('message_type'),
            'message' => $request->get('message'),
        ];

        LogMessage::create($obj);

        $payload = JWTAuth::parseToken()->getPayload();
        $device = $payload->get('device_id');

        return Response::json(['status'=>0],200);
    }

    public function list(){
        return LogMessage::all();
    }
}
