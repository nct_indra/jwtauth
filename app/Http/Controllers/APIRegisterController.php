<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use App\User;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;


class APIRegisterController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required',
            'password'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $register = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ];

        User::create($register);

        $user = User::first();

        $token = JWTAuth::fromUser($user);
        //$token = JWTAuth::attempt($register, ['exp' => 1 ]);
        
        return Response::json(compact('token'));
    }
}
