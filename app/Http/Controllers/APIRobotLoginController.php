<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APIRobotLoginController extends Controller
{
    public function login(Request $request)
        {

            $validator = Validator::make($request->all(), [
                'email_address' => 'required|string|email|max:255',
                'device_id' => 'required',
                'password'=> 'required'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }
            $credentials = $request->only('email', 'password', 'device_id');
            $customClaims = [
                'exp' => Carbon::now()->addMinutes(1)->timestamp,
                'device_id' => $request->get("device_id")
            ];
            try {
                if (! $token = JWTAuth::attempt($credentials, $customClaims)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }
            return response()->json(compact('token'));
        }
}
