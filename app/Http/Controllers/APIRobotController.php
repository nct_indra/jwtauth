<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use App\User;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Carbon\Carbon;


class APIRobotController extends Controller
{
    public function register(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|string|email|max:255|unique:users',
                'password'=> 'required'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $register = [
                'username' => $request->get('username'),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
                'activated' => "1",
            ];

            $userId = \DB::table('users')->insertGetId(
                [
                     'username' => $request->get('username'),
                                    'first_name' => $request->get('first_name'),
                                    'last_name' => $request->get('last_name'),
                                    'email' => $request->get('email'),
                                    'password' => bcrypt($request->get('password')),
                                    'activated' => "1",
                ]
            );

            $user = User::find($userId);

            //$user = User::create($register);

            \DB::table('role_user')->insert([
                [
                'role_id' => '2',
                'user_id' => $userId
                ]
            ]);

            $token = JWTAuth::fromUser($user);

            return Response::json(compact('token'));
        }

    public function generateToken(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'device_id'=> 'required'
        ]);

        $user = \DB::table('users')->where('email', $request->get('email'))->first();

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()]);
        }

        $credentials = $request->only($user->email, $user->password);
            $customClaims = [
                'exp' => Carbon::now()->addMinutes(60)->timestamp,
                'device_id' => $request->get("device_id")
            ];
        try {
            if (! $token = JWTAuth::fromUser($user, $customClaims)) {
                return response()->json(['error' => 'invalid_credentials', 'password' => $user->password], 401);
            }
        } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }
}
