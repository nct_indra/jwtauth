<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogMessage extends Model
{
    //
    protected $fillable = ['message_type', 'message'];
}
